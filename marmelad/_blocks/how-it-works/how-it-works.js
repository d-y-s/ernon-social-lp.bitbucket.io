
var howItWorks = tns({
    container: '.js-container-img',
    items: 1,
    axis: 'vertical',
    slideBy: 'page',
    autoplay: false,
    swipeAngle: 'false',
    nav: false,
    controls: false,
    speed: 500
});

$('.js-item-theme').each(function (index, el) {
    var $thisheight = $(this).outerHeight();

    if (index === 0) {
        $('.js-active-border').css('height', $thisheight);
        $('.js-active-border').css('top', $(this).position().top);
    }

    $(this).on('click', function () {

        $('.js-active-border').css('height', $thisheight);

        $('.js-active-border').css('top', $(this).position().top);
    })
});

$('.js-item-theme').on('click', function () {
    $(this)
        .addClass('is-active-item-theme')
        .siblings()
        .removeClass('is-active-item-theme');
    howItWorks.goTo($(this).index()-1);
});

if ($(window).width() <= 767) {

    howItWorks.destroy();

    $('.js-active-border').remove();

    var howItWorks = tns({
        container: '.js-container-img',
        items: 1,
        slideBy: 'page',
        autoplay: false,
        swipeAngle: false,
        touch: true,
        nav: true,
        navPosition: 'bottom',
        controls: true,
        speed: 500,
        controlsText: ["&lsaquo;", "&rsaquo;"],
    });

    howItWorks.events.on('transitionStart', function () {
        var info = howItWorks.getInfo();
        var index = info.displayIndex - 1;

        $('.js-item-theme')
            .eq(index)
            .addClass('is-active-item-theme').siblings()
            .removeClass('is-active-item-theme');
    });

}