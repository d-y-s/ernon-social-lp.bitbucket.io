var socialBlock = tns({
    container: '.js-social-block',
    items: 4,
    axis: 'vertical',
    slideBy: 'page',
    autoplay: true,
    autoplayTimeout: 2000,
    autoplayHoverPause: false,
    swipeAngle: false,
    autoHeight: false,
    touch: false,
    nav: false,
    autoplayButtonOutput: false,
    controls: false,
    speed: 500,
    slideBy: 1,
    responsive: {
        320: {
            items: 2,
        },
        1251: {
            items: 4,
        }
    },

    onInit: function () {

        if ($(window).width() >= 1251) {

            $('.social-block__item').removeClass('is-active-index');
            $('.js-social-block').find('.tns-slide-active').each(function (index, el) {
    
                var thisIndex = $(this).data('index');
    
                
                $('.social-block__item[data-index="' + thisIndex + '"]').addClass('is-active-index');
                
            })
        }
    }

});

if ($(window).width() >= 1251) {
    socialBlock.events.on('transitionStart', function () {
        $('.social-block__item').removeClass('is-active-index');
        $('.js-social-block').find('.tns-slide-active').each(function (index, el) {
            
            var thisIndex = $(this).data('index');

            $('.social-block__item[data-index="' + thisIndex +'"]').addClass('is-active-index');

        })
    });

   
}

if ($(window).width() <= 1250) {

    $('.social-block').insertBefore('.first-screen__btn-free');
    
}