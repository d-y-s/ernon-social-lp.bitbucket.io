if ($(window).width() <= 640) {
    var socialTaskMob = tns({
        container: '.js-social-tasks-mob',
        items: 1,
        axis: 'horizontal',
        slideBy: 'page',
        autoplay: false,
        autoplayTimeout: false,
        autoplayHoverPause: false,
        swipeAngle: false,
        touch: true,
        nav: false,
        autoplayButtonOutput: false,
        controls: true,
        controlsText: ["&lsaquo;", "&rsaquo;"],
        speed: 800,
    });
}