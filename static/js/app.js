"use strict";

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

/* ^^^
 * Глобальные-вспомогательные функции
 * ========================================================================== */

/* ^^^
 * Viewport Height Correction
 *
 * @link https://www.npmjs.com/package/postcss-viewport-height-correction
 * ========================================================================== */
function setViewportProperty() {
  var vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty('--vh', vh + 'px');
}

window.addEventListener('resize', setViewportProperty);
setViewportProperty(); // Call the fuction for initialisation

/* ^^^
 * Возвращает HTML-код иконки из SVG-спрайта
 *
 * @param {String} name Название иконки из спрайта
 * @param {Object} opts Объект настроек для SVG-иконки
 *
 * @example SVG-иконка
 * getSVGSpriteIcon('some-icon', {
 *   tag: 'div',
 *   type: 'icons', // colored для подключения иконки из цветного спрайта
 *   class: '', // дополнительные классы для иконки
 *   mode: 'inline', // external для подключаемых спрайтов
 *   url: '', // путь до файла спрайта, необходим только для подключаемых спрайтов
 * });
 */

function getSVGSpriteIcon(name, opts) {
  opts = _extends({
    tag: 'div',
    type: 'icons',
    "class": '',
    mode: 'inline',
    url: ''
  }, opts);
  var external = '';
  var typeClass = '';

  if (opts.mode === 'external') {
    external = "".concat(opts.url, "/sprite.").concat(opts.type, ".svg");
  }

  if (opts.type !== 'icons') {
    typeClass = " svg-icon--".concat(opts.type);
  }

  opts["class"] = opts["class"] ? " ".concat(opts["class"]) : '';
  return "\n    <".concat(opts.tag, " class=\"svg-icon svg-icon--").concat(name).concat(typeClass).concat(opts["class"], "\" aria-hidden=\"true\" focusable=\"false\">\n      <svg class=\"svg-icon__link\">\n        <use xlink:href=\"").concat(external, "#").concat(name, "\"></use>\n      </svg>\n    </").concat(opts.tag, ">\n  ");
}
/* ^^^
 * JQUERY Actions
 * ========================================================================== */


$(function () {
  'use strict';
  /**
   * определение существования элемента на странице
   */

  var _tns;

  $.exists = function (selector) {
    return $(selector).length > 0;
  };

  var howItWorks = tns({
    container: '.js-container-img',
    items: 1,
    axis: 'vertical',
    slideBy: 'page',
    autoplay: false,
    swipeAngle: 'false',
    nav: false,
    controls: false,
    speed: 500
  });
  $('.js-item-theme').each(function (index, el) {
    var $thisheight = $(this).outerHeight();

    if (index === 0) {
      $('.js-active-border').css('height', $thisheight);
      $('.js-active-border').css('top', $(this).position().top);
    }

    $(this).on('click', function () {
      $('.js-active-border').css('height', $thisheight);
      $('.js-active-border').css('top', $(this).position().top);
    });
  });
  $('.js-item-theme').on('click', function () {
    $(this).addClass('is-active-item-theme').siblings().removeClass('is-active-item-theme');
    howItWorks.goTo($(this).index() - 1);
  });

  if ($(window).width() <= 767) {
    howItWorks.destroy();
    $('.js-active-border').remove();
    var howItWorks = tns({
      container: '.js-container-img',
      items: 1,
      slideBy: 'page',
      autoplay: false,
      swipeAngle: false,
      touch: true,
      nav: true,
      navPosition: 'bottom',
      controls: true,
      speed: 500,
      controlsText: ["&lsaquo;", "&rsaquo;"]
    });
    howItWorks.events.on('transitionStart', function () {
      var info = howItWorks.getInfo();
      var index = info.displayIndex - 1;
      $('.js-item-theme').eq(index).addClass('is-active-item-theme').siblings().removeClass('is-active-item-theme');
    });
  }

  var PAGE = $('html, body');
  var pageScroller = $('.page-scroller');
  var inMemoryClass = 'page-scroller--memorized';
  var isVisibleClass = 'page-scroller--visible';
  var enabledOffset = 60;
  var pageYOffset = 0;
  var inMemory = false;

  function resetPageScroller() {
    setTimeout(function () {
      if (window.pageYOffset > enabledOffset) {
        pageScroller.addClass(isVisibleClass);
      } else if (!pageScroller.hasClass(inMemoryClass)) {
        pageScroller.removeClass(isVisibleClass);
      }
    }, 150);

    if (!inMemory) {
      pageYOffset = 0;
      pageScroller.removeClass(inMemoryClass);
    }

    inMemory = false;
  }

  if (pageScroller.length > 0) {
    window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
      passive: true
    } : false);
    pageScroller.on('click', function (event) {
      event.preventDefault();
      window.removeEventListener('scroll', resetPageScroller);

      if (window.pageYOffset > 0 && pageYOffset === 0) {
        inMemory = true;
        pageYOffset = window.pageYOffset;
        pageScroller.addClass(inMemoryClass);
        PAGE.stop().animate({
          scrollTop: 0
        }, 500, 'swing', function () {
          window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
            passive: true
          } : false);
        });
      } else {
        pageScroller.removeClass(inMemoryClass);
        PAGE.stop().animate({
          scrollTop: pageYOffset
        }, 500, 'swing', function () {
          pageYOffset = 0;
          window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
            passive: true
          } : false);
        });
      }
    });
  }

  var socialBlock = tns((_tns = {
    container: '.js-social-block',
    items: 4,
    axis: 'vertical',
    slideBy: 'page',
    autoplay: true,
    autoplayTimeout: 2000,
    autoplayHoverPause: false,
    swipeAngle: false,
    autoHeight: false,
    touch: false,
    nav: false,
    autoplayButtonOutput: false,
    controls: false,
    speed: 500
  }, _defineProperty(_tns, "slideBy", 1), _defineProperty(_tns, "responsive", {
    320: {
      items: 2
    },
    1251: {
      items: 4
    }
  }), _defineProperty(_tns, "onInit", function onInit() {
    if ($(window).width() >= 1251) {
      $('.social-block__item').removeClass('is-active-index');
      $('.js-social-block').find('.tns-slide-active').each(function (index, el) {
        var thisIndex = $(this).data('index');
        $('.social-block__item[data-index="' + thisIndex + '"]').addClass('is-active-index');
      });
    }
  }), _tns));

  if ($(window).width() >= 1251) {
    socialBlock.events.on('transitionStart', function () {
      $('.social-block__item').removeClass('is-active-index');
      $('.js-social-block').find('.tns-slide-active').each(function (index, el) {
        var thisIndex = $(this).data('index');
        $('.social-block__item[data-index="' + thisIndex + '"]').addClass('is-active-index');
      });
    });
  }

  if ($(window).width() <= 1250) {
    $('.social-block').insertBefore('.first-screen__btn-free');
  }

  if ($(window).width() <= 640) {
    var socialTaskMob = tns({
      container: '.js-social-tasks-mob',
      items: 1,
      axis: 'horizontal',
      slideBy: 'page',
      autoplay: false,
      autoplayTimeout: false,
      autoplayHoverPause: false,
      swipeAngle: false,
      touch: true,
      nav: false,
      autoplayButtonOutput: false,
      controls: true,
      controlsText: ["&lsaquo;", "&rsaquo;"],
      speed: 800
    });
  }

  $("#burger").on('click', function () {
    $(this).toggleClass("is-active-burger");
    $('.js-mobile-menu').toggleClass("is-open-mobile-menu");
  });
});